'use strict'

const realtime = require('./realtime');
const fs = require('fs');
const https = require('https');
const express = require('express');
const app = express();

const options = {
  key: fs.readFileSync('/etc/letsencrypt/archive/app.zicomarket.com/privkey1.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/archive/app.zicomarket.com/cert1.pem')
};

const server = https.createServer(options, app);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

server.listen(3000, function() {
    console.log('server');
});

realtime(server);
