'use strict'

const socketio = require('socket.io');
const redis = require('redis');

module.exports = function(server){
    const io = socketio(server);

    const redisClient = redis.createClient();
    let aux_sala;

    redisClient.subscribe('app_zicochat');

    io.on('connection', function(socket) {
        console.log('new client connected');
        io.emit('testing room', "usuario conectado");

        /*
            socket.on('create', function(room) {
                console.log("create room ..");
                socket.join(room);
            });

            socket.on('message',function(data){
                // console.log("mensage XDD ..."+data["message"]+data["room"]);
                //temp();
                console.log("------------------->");
            });

            const redisClient = redis.createClient();
            let aux_sala;

            redisClient.subscribe('app_zicochat');
        */
    });


    redisClient.on('message', function(channel, message) {
        console.log('new message in queue', channel, message);

        let data = JSON.parse(message);
        //channel
        io.emit(data.room, data);
    });

}