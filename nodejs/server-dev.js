'use strict'

const realtime = require('./realtime');

const app = require('express')();
const server = require('http').Server(app);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

server.listen(3000, function() {
    console.log('server');
});

realtime(server);
