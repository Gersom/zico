# Title
<!--  -->

----

## 1. [GET] Home
  <!--  -->

### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->
  ```javascript
  [GET]
  `/api/${VERSION}/items/`
  `/api/${VERSION}/items?sector_id={}`

  [POST]
  `/api/${VERSION}/items`

  [PUT]
  `/api/${VERSION}/items/${id}`

  [DELETE]
  `/api/${VERSION}/items/${id}`
  ```

  * #### Method:

    <!-- <_The request type_> -->

    `GET` | `POST` | `DELETE` | `PUT`

  * #### Params

    <!-- <_If URL params exist>  -->

    * __Required:__

      `id=[integer]` <br />

    * __Optional:__

     `category=[alphanumeric]` <br />

  * #### Queries

    <!-- <_If URL params exist_>  -->

    * __Required:__

      `height=[integer]` <br />

    * __Optional:__

      `width=[integer]` <br />


### HEADERS
  * #### Content:

  ```json
    {
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "csrftoken": "{TOKEN}",
        "language": "es"
      }
    }
  ```

### REQUEST

  <!--  -->

  * #### Content:

  ```json
    {
      "username": "example",
      "password": "12345678"
    }
  ```

### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "id": "",
          "imageUrl": ""
        }
      ]
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.
