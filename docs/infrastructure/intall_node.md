Intall Node
===========

//ingresar a la carpeta donde esta el servidor node
    cd /..../appzicomarket/nodejs

//si es la primera vez se tiene que ejecutar:
    npm install

// se instalaran los recursos necesarios para ejecutar el servidor de node
// Cuando termine de descargar las librerias necesarias, se tiene
// que prender el servidor de node

    node server.js

    **- ref: https://docs.npmjs.com/cli/install -**


Estructura de archivos nodejs
=============================
// Existen dos archicos server.js y server-dev.js los cuales serán usados en entorno de
// producción y desarrollo respectivamente.
// Ambos tienen su apropiada configuración y requieren al archivo 'realtime.js',
// donde se encuentra el funcionamiento del Zicochat.

Node corriendo como https (server.js)
=====================================
// Requerir los modulos 'fs' y 'https' de node (ya vienen incluidos).
// 'fs' leera las llaven de ssl, previamente se debe dar permisos de lectura (755)
// a la carpeta que las contenga.
// 'https' creará un servidor seguro para producción.