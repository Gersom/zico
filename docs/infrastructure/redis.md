Nota: la instalación depende del sistema operativo que se tenga instalado en la maquina.
## install redis ubuntu

    sudo apt-get update
    sudo apt-get install build-essential
    sudo apt-get install tcl8.5

    wget http://download.redis.io/releases/redis-stable.tar.gz
    tar xzf redis-stable.tar.gz
    cd redis-stable
    make
    make test
    sudo make install
    cd utils
    sudo ./install_server.sh
    sudo service redis_6379 start
    sudo service redis_6379 stop
    redis-cli
        -> redis 127.0.0.1:6379>
    **- ref: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-redis --**

## install redis debian Manually Install Redis 3+

    sudo apt-get update
    sudo apt-get install build-essential -y

    wget http://download.redis.io/releases/redis-stable.tar.gz
    tar xzf redis-stable.tar.gz
    cd redis-stable
    make
    sudo apt-get install tcl8.5 -y
    # optionally run "make test" to check everything is ok
    sudo make install
    cd utils
    sudo ./install_server.sh
    redis-server --version
    sudo service redis_6379 [start | stop | restart]
    redis-cli
        -> redis 127.0.0.1:6379>

    **- ref: http://blog.programster.org/debian-8-install-redis-server/ -**
