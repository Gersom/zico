# Routes

----

## 1. Home

  <!--  -->

  * #### Content:

    ```javascript
      PATH = `/${langCode}/`
      PARAMS = {}
    ```

## 2. Login

  <!--  -->

  * #### Content:

    ```javascript
      PATH = `/${langCode}/login`
      PARAMS = {}
    ```

## 3. Offers

  <!--  -->

  * #### Content:

    ```javascript
      PATH = `/${langCode}/offers`

      PARAMS = {
        'region': Number, // optional
        'sector': Number // optional
      }
    ```

## 3. Offer Profile

  <!--  -->

  * #### Content:

    ```javascript
      PATH = `/${langCode}/offers/${offerSlug}`

      PARAMS = {}
    ```

## 4. Companies / Directorio

  <!--  -->

  * #### Content:

    ```javascript
      PATH = `/${langCode}/companies/`

      PARAMS = {
        'region': Number, // optional
        'sector': Number // optional
      }
    ```

## 5. Company Profile

  <!--  -->

  * #### Content:

    ```javascript
      PATH = `/${langCode}/companies/${companySlug}`

      PARAMS = {
        'region': Number, // optional
        'sector': Number // optional
      }
    ```

## 6. Zico Chat

  <!--  -->

  * #### Content:

    ```javascript
      PATH = `/${langCode}/zicochat`

      PARAMS = {}
    ```
