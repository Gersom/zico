# LANDING

----

## 1. [GET] countries


### PATH

  ```javascript
  // normal
  `/api/${VERSION}/countries`

  // filtado por el campo visible
  `/api/${VERSION}/countries?visible=${true || false}`
  ```

  * #### Method: `GET`

  * #### Queries

    * __Optional:__

      `visible = [boolean]`



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "id": 0,
          "image_url": "",
          "name": "",
          "number_companies": 999
        }
      ]
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



### Notes:

  Visible = true o false, si no hay parametro envio todos ...
