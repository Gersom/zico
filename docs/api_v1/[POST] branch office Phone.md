# BRANCH OFFICE PHONE

----

## 1. [post] branch-office-phone


### PATH

  ```javascript
  `/api/${VERSION}/branch-office-phone`
  ```

  * #### Method: `PUT`


### REQUEST

    * #### Content:

    ```json
      {
          "id":1,
          "phones": [
            { "main": true, "value": "1234" },
            { "main": true, "value": "1234" }
          ]
      }
    ```

### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json

     {
         "success":true,         
     }

    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

notes:
    todo es requerido
