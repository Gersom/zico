# PUBLIC

----

## 1. [GET] Districts



### PATH

  ```javascript
  `/api/${VERSION}/districts?province=${0}`
  ```

  * #### Method: `GET`

  * #### Queries

    * __Optional:__

      `provinces = [integer]`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "id": 0,
          "name": ""
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />



### Notes:

  Filtrar los distritos por el id de la provincia, si es requerido

province = 0 || '' entonces todos los distritos;
