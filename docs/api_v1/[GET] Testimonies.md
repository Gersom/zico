# PUBLIC

----

## 1. [GET] Testimonies



### PATH

  ```javascript
  `/api/${VERSION}/testimonies`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "description": "",
          "fullname": "",
          "image_url": "",
          "position": "",
          "video_code": "",
          "video_domain": ""
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

    notes:

    no me especifico que calidad de imagen quiere que le envie ...
    
