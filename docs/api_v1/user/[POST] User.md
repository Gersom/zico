# Title
<!--  -->

----

## 1. [GET] Home
  <!--  -->

### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->
  ```javascript  
  [POST]
  `/api/${VERSION}/user`

  * #### Method:

    <!-- <_The request type_> -->

    `POST`


### HEADERS
  * #### Content:

  ```json
    {
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "{TOKEN}",
        "language": "es"
      }
    }
  ```

### REQUEST

  <!--  -->

  * #### Content:

  ```json
    {
      "serial": "12345678",
      "type": 1,
      "first_name": "name_test",
      "last_name": "apellido",
      "gender": 1,
      "birthday": "1987-06-06",
      "country": 1,
      "region": 1,
      "address": "av. testing s/n",
      "company_id": "1",
      "sector_position": "Asistente ...",
      "email": "123@123.com",
      "phone": "963 847 125",
      "avatar": "https://andfandfosf.com",


      "username": "jonathanXD",
      "password": "12345678",
      "password2": "12345678",
      "role": 1,      

      "status": 1,
    }
  ```

### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "success": true,          
        }
      ]
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "error": false,
      "message": "no existe la compañia xd referenciada ..."
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  crea un usuario para testing ...
