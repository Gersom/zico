# USER PROFILE
<!--  -->

----

## 1. [PUT] user profile



### PATH

  ```javascript  
  `/api/${VERSION}/user-profile`
  ```

  * #### Method: `PUT`



### REQUEST

  * #### Content:

  ```json
  {
    "document_id": 1,
    "serial_identity": 12345678,
    "first_name": "demo update",
    "last_name": "demo update last name",
    "gender": 1,
    "birthday": "2015-01-02",
    "cellphones": [
      { "id": 0, "main": true, "value": "1234" },
      { "id": 0, "main": true, "value": "1235" }
    ],
    "description": "",
    "email": "demo@demo.com",
    "country_id": 1,
    "region_id": 1,
    "province_id": 1,
    "district_id": 1,
    "address": "Av. demo",
    "photo_src": "[base64]",
    "photo_type": "png || jpg || ..."
  }
  ```



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success" : true
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "error": false,
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  Aun no se define como actualizar redes sociales  ...
