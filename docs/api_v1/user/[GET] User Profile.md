# User Profile

<!--  -->

----

## 1. [GET] User Profile

  Datos del usuario que se mostraran en el header



### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->

  ```javascript
  [GET] `/api/${VERSION}/user-profile/`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "company": {
          "id": 1,
          "name": "",
          "slug": ""
        },
        "conversations": true,
        "first_name": "",
        "image_200px": "",
        "last_name": "",
        "role": 1,
        "userId": 3
      }
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado <br />
    __content:__

    ```json
      {
        "error": "",
        "message": ""
      }
    ```


<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->
