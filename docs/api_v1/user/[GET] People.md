# User Profile

<!--  -->

----

## 1. [GET] User Profile



### PATH

  ```javascript
  `/api/${VERSION}/people`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "document_id": 1,
        "serial_identity": 12345678,
        "first_name": "demo update",
        "last_name": "demo update last name",
        "gender": 1,
        "birthday": "2015-01-02",
        "cellphones": [
          { "id": 0, "main": true, "value": "1234" },
          { "id": 0, "main": false, "value": "1235" }
        ],
        "description": "",
        "email": "demo@demo.com",
        "country_id": 1,
        "region_id": 1,
        "province_id": 1,
        "district_id": 1,
        "address": "Av. demo",
        "photo_src": "[URL]"
      }
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado <br />
    __content:__

    ```json
      {
        "error": "",
        "message": ""
      }
    ```


### Notes:

  Necesitas estar logeado para que retorne info...
