# Companies

<!--  -->

----

## 1. [GET] Companies

  <!--  -->



### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->

```javascript
  [GET] `/api/${VERSION}/companies/${Slug}/`
```

  * #### Method:

    <!-- <_The request type_> -->

    `GET`

  * #### Params

    <!-- <_If URL params exist>  -->

    * __Required:__

      `companySlug=[string]` <br />



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "description": "",
        "id": 0,
        "image_200px": "",
        "image_75px": "",
        "name": "",
        "qualification": 1,
        "sector": {
          "id": 0,
          "name": "",
          "image": ""
        },
        "region_name": "",
        "country_name": "",
        "creation": "[DATE]",

        "region_id": "",
        "country_id": "",
        "gain_id": 0,
        "employees": 0,
        "numberIdentity": "",

        "business_reasons": "",
        "branch_offices_countries": ["", "", ""],
        "main_customers": "",
        "certifications": "",
        "affiliations": "",
        "export_countries": "",
        "importing_countries": "",
        "regions_interest": "",
        "slug": "",

        "items": [
          {
            "date_publish": "21 Jul",
            "id": "",
            "image_200px": "",
            "name": "",
            "slug": "",
            "region_name": "",
            "country_name": ""
          }
        ]
      }
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```


### Notes:

  No interesa que no cuente con imagenes la compañia u ofertas disponibles.
