# Companies

<!--  -->

----

## 1. [GET] Companies

  <!--  -->



### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->

  ```javascript
  [GET]
  `/api/${VERSION}/companies/`
  `/api/${VERSION}/companies?sector_id={}&region_id={}/`
  ```

  * #### Method:

    <!-- <_The request type_> -->

    `GET`


  * #### Queries

    <!-- <_If URL params exist_>  -->

    * __Optional:__

      `region_id=[integer]` <br />
      `sector_id=[integer]` <br />



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "country_id": 0,
          "country_name": "",
          "id": 0,
          "image_200px": "",
          "name": "",
          "region_name": "",
          "sector_id": 0,
          "sector_name": 0,
          "slug": ""
        }
      ]
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```


<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->
