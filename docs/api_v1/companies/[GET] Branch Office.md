# COMPANY

----

## 1. [GET] Create Branch Offices

### PATH

  ```javascript
  `/api/${VERSION}/branch-office/${id}`
  ```

  * #### Method: `GET`

  * #### Params    

    * __Required:__

      `id = [integer]`



### REQUEST

  * #### Content:

  ```json
    {
      "company_id":1,
      "country_id": 1,
      "name": "",
      "main":1,
      "region_id": 1,
      "province_id": 1,
      "district_id": 1,
      "address": "av. lalala",
      "representative": "lalalla",
      "phones": [
        { "main": true, "value": "1234" },
        { "main": false, "value": "1234" }
      ],
      "cellphones": [
        { "main": true, "value": "1234" },
        { "main": true, "value": "1234" }
      ],
      "email": "asjdfajsdf@gmail.com",
      "website": "www.google.com",
      "social": [
        {   "name": "facebook",
            "logo": "www.as.com.png",
            "profile": "fb.com/asd"
        },
        {   "name": "twitter",
            "logo": "www.as.com.png",
            "profile": "tw.com/asd"
        }
      ],
      //"facebook": "fb.com/asd",
      //"twitter": "tw.com/asd",
    }
  ```



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success" : true
    }
    ```

  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1' o ro '2'


### Notes:

  Pedir datos de un branch_offices
