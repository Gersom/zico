# COMPANIES

----

## 1. [PUT] EDIT COMPANY

### PATH

  ```javascript
  [PUT]
  `/api/${VERSION}/companies`
  ```

  * #### Method: `PUT`



### REQUEST

  * #### Content:

  ```javascript
    {
      "id": 1,                
      "name": "Name Test",
      "country_id": 1,
      "region_id":1,
      "sector_id":1,
      "number_identity":"009234234",
      "creation": "10-09-2010",
      // formato de creation =  año-mes-dia
      // numero de empleados
      "employees": 10,
      // rango de ganancia
      "gain_id": 1,

      "description": "lalalalalalla",

      // describe lo que le intereza saber al negociar con una empresa
      "business_reasons": "lol",
      // Regiones de interes
      "regions_interest": "lol",
      // principales clientes
      "main_customers": "lol",
      // Certificaciones
      "certifications": "lol",
      // paises de exportación
      "export_countries": "lol",
      // paises de importacion
      "importing_countries": "lol",
      // paises a los que exporta 2 XD
      "affiliations": "lol",

      "file":"base64",
      "file_type":"pdf|jpg|png",
      "file_brochure":"base64",
      "file_type_brochure":"pdf|jpg|png"
    }
  ```



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success" : true
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1' o ro '2'


### Notes:

  Actualiza la información de la empresa
