# Languages

<!--  -->

----

## 1. [GET] Languages

  <!-- A las __CATEGORIAS__ en zicomarket se les llama __SECTORES__ -->



### PATH

  <!--  -->

  ```javascript
  [GET] `/api/${VERSION}/languages/`
  ```

  * #### Method:

    <!-- <_The request type_> -->

    `GET`



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "code": "",
          "id": "",
          "name": ""
        }
      ]
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->
