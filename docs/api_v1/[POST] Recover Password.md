# LOGIN

----

## 1. [POST] Recover Password

### PATH

  ```javascript
  `/api/${VERSION}/recover-password/`
  ```

  * #### Method: `POST`



### REQUEST

  * #### Content:

  ```json
    {
      "serial_identity": ""
    }
  ```



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "email": "",
      },
      "Success": true
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no EXISTE dicho usuario <br />

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el usuario esta deshabilitado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



### Notes:

  404 el usuario no EXISTE
  403 el usuario esta bloqueado 
