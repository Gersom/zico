# Specific Item

<!--  -->

----

## 1. [GET] Specific Item

  <!--  -->



### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->

```javascript
  [GET] `/api/${VERSION}/items/${itemSlug}/`
```

  * #### Method:

    <!-- <_The request type_> -->

    `GET`

  * #### Params

    <!-- <_If URL params exist>  -->

    * __Required:__

      `itemId=[integer]` <br />



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "applications": "",
        "attributes": "",
        "coin": "",
        "description": "",
        "id": 0,
        "image_550px": [
          {
            "id": 0,
            "url": "[URL]"
          }
        ],
        "name": "",
        "params": "",
        "price": "",
        "references": "",
        "sale": "",
        "sector_name": "",
        "sector_id":1,
        "company": {
          "name": "",
          "image_200px": "",
          "id": "",
          "slug": "",
        },
        "users": [
          {
            "first_name": "",
            "image_200px": "",
            "last_name": "",
            "stall": ""
          }
        ]
      }
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```


<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->
