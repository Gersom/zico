# Items

<!--  -->

----

## 1. [GET] Items

  <!--  -->



### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->

  ```javascript
  // Todos los items
  [GET]
  `/api/${VERSION}/items/`
  `/api/${VERSION}/items?sector_id=0&region_id=0/`
  `/api/${VERSION}/items?q=0`
  ```

  * #### Method:

    <!-- <_The request type_> -->

    `GET`


  * #### Queries

    <!-- <_If URL params exist_>  -->

    * __Optional:__

      `region_id=[integer]` <br />
      `sector_id=[integer]` <br />



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "company_name": "",
          "company_slug": "",
          "date_publish": "21 Jul",
          "id": 0,
          "image_200px": "",
          "name": "",
          "slug": "",
          "region_name": "",
          "country_name": ""
        }
      ]
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->
