# Title
<!--  -->

----

## 1. [POST] Item
  <!--  -->

### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->
  ```javascript
  [POST]
  `/api/${VERSION}/items`
  ```

  * #### Method:

    <!-- <_The request type_> -->

    `POST`

  * #### Params

    <!-- <_If URL params exist>  -->


    <!-- * __Optional:__

     `category=[alphanumeric]` <br /> -->

  <!-- * #### Queries

    <!-- <_If URL params exist_>  -->

    <!-- * __Required:__ -->

      <!-- `height=[integer]` <br />

    * __Optional:__

      `width=[integer]` <br /> -->


### HEADERS
  * #### Content:

  ```json
    {
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "csrftoken": "{TOKEN}",
        "language": "ES"||"EN"
      }
    }
  ```
### REQUEST

    <!--  -->

    * #### Content:

    ```json
    {
        "name": "aceite de oliva",
        "sector": "1",        
    }
    ```
### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success": true
      "data":{
          "id":6,
          "name":"aceite de oliva",
          "slug":"aceite-de-oliva"
      }
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  Crea un registro de offerta (item) en un  idioma especificado.
