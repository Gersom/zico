# Title
<!--  -->

----

## 1. [DELETE] Item
  <!--  -->

### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->
  ```javascript

  [DELETE]
  `/api/${VERSION}/items/${id}`
  ```

  * #### Method:

    <!-- <_The request type_> -->

    `DELETE`

  * #### Params

    <!-- <_If URL params exist>  -->

    * __Required:__

      `id=[integer]` <br />


  * #### Queries

    <!-- <_If URL params exist_>  -->


### HEADERS
  * #### Content:

  ```json
    {
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "csrftoken": "{TOKEN}",
        "language": "es"
      }
    }
  ```

### REQUEST

  <!--  -->

  * #### Content:

  ```json
    {
      "username": "example",
      "password": "12345678"
    }
  ```

### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "id": "",
          "imageUrl": ""
        }
      ]
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.
