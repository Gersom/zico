# Items

----

## 1. [PUT] Item

### PATH

  ```javascript
  `/api/${VERSION}/news`
  ```

  * #### Method: `PUT`



### HEADERS
  * #### Content:

  ```json
    {
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "csrftoken": "{TOKEN}",
        "language": "ES"||"EN"
      }
    }
  ```



### REQUEST

* #### Content:
  <!-- name_camp = description,applications, references, params, attributes, sales, coin,price -->

  ```json
      {
      "content": "asjdbfasdf",
      "title": "sdfasdf",
      "index": "1",      
      "tag_slug": ""
      }
  ```

### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success": true || false
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  Actualiza un campo en específico de cualquier oferta .
