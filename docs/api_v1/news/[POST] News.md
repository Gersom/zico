# NEWS

----

## 1. [POST] news

### PATH

  ```javascript
  `/api/${VERSION}/news`
  ```

  * #### Method: `POST`



### HEADERS

  * #### Content:

  ```json
    {
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "csrftoken": "{TOKEN}",
        "language": "ES"||"EN"
      }
    }
  ```



### REQUEST

    * #### Content:

    ```json
    {
        "title": "aceite de oliva",
        "content": "aceite de oliva",
        "image": {
          "src": "",
          "extension": ""
        },
        "tagSlug": ""
    }
    ```



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success": true,
      "data": {
        "slug": ""
      }
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  Crea un registro de offerta (item) en un  idioma especificado.
  Nota: la fecha no es editable
  Solo el rol uno crea noticias ....
