# NEWS

----

## 1. [GET] News



### PATH

  ```javascript
  `/api/${VERSION}/news/${slug}?max_others=${n}`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "date_published": "",
          "id": 0,
          "image_url_200px": "",
          "image_url_550px": "",
          "image_url": "",
          "image_id": "",
          "title": "",
          "content": "[HTML]",
          "others": [
            {
              "date_published": "",
              "id": 0,
              "image_url_200px": "",
              "image_url": "",
              "slug": "",
              "title": ""
            }
          ],
          "tag": {
            "name": "",
            "slug": ""
          }
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR



### Notes:

  max_others indica la cantidada mazimo de elementos que tiene tener el array others
  others: otras noticias listadas, ordenadas por fecha de publicación No necesita logeado.
  si no hay max_other envio todo
