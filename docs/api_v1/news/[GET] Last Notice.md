# NEWS

----

## 1. [GET] News



### PATH

  ```javascript
  `/api/${VERSION}/news/last`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "date_published": "",
        "id": 0,
        "image_url_200px": "",
        "image_url": "",
        "slug": "",
        "title": ""
      }
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />


### Notes:

Sin logeado.
