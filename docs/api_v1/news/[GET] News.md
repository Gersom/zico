# NEWS

----

## 1. [GET] News



### PATH

  ```javascript
  // All
  `/api/${VERSION}/news`
  `/api/${VERSION}/news?max_elements=${n}`
  `/api/${VERSION}/news?tag=${tagSlug}`
  `/api/${VERSION}/news?max_elements=${n}&tag=${tagSlug}`
  ```

  * #### Method: `GET`

  * #### Queries

    * __Optional:__

      `max_elements=[integer]` <br />
      `tag=[integer]` <br />



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "date_published": "",
          "id": 0,
          "image_url_200px": "",
          "image_url": "",
          "slug": "",
          "title": ""
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />




### Notes:

  max_elements indica la cantidada maxima de elementos que tiene tener el array
  si no hay max_other envio todo
