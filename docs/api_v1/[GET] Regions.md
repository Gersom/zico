# Regions

----

## 1. [GET] Regions

### PATH

  ```javascript
  // filtrado por pais
  [GET] `/api/${VERSION}/regions?country=${0}`

  // Solo regiones con items
  [GET] `/api/${VERSION}/regions?visible=${false}`
  ```

  * #### Method: `GET`

  * #### Queries

    * __Optional:__

      `country = [integer]`



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "countryId": "",
          "countryName": "",
          "id": 0,
          "image": "",
          "name": "",
          "slug": ""
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



### Notes:

  filtrar las regiones por el id del pais, si es requerido
