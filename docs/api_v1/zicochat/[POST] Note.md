# ZICOCHAT

----

## 1. [POST] Notes

### PATH

  ```javascript
  [POST]
  `/api/${VERSION}/zicochat/notes`
  ```

  * #### Method: `POST`

### REQUEST

  <!--  -->

  * #### Content:

  ```json
    {
      "romm_name": "9e5e9bdc-0518-44b5-abcf-2a851b841b3c1",
      "content": "Es el contenido de la nota"
    }
  ```

### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "id": "",
        "content": "",
        "time": "20:15",
        "date": " 22 Junio"
      },
      "success": true
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "message": "",
      "success": false,
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado


### Notes:
