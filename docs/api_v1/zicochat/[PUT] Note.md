# ZICOCHAT

----

## 1. [PUT] Notes



### PATH

  ```javascript

  [PUT]
  `/api/${VERSION}/zicochat/notes`
  ```

  * #### Method: `PUT`

### REQUEST

  <!--  -->

  * #### Content:

  ```json
    {
      "id": 1,
      "content": "Es el contenido de la nota modificado"
    }
  ```

### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "id": "",
        "content": "",
        "time": "20:15",
        "date": " 22 Junio"
      },
      "success": true
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "message": "",
      "success": false,
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado


### Notes:
