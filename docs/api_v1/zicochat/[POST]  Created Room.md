# Zicochat
<!--  -->

----

## 1. [POST] Room
  Crear una nueva sala

### PATH

  ```javascript

  [POST]
  `/api/${VERSION}/zicochat/room/{user_id}`
  ```

  * #### Method: `POST`

  * #### Params

    * __Required:__

      `companyId=[integer]` <br />



### REQUEST -->

  * #### Content:

  ```json
    {}
  ```



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success": true,
      "data": {
          "company_id": 3,
          "first_name": "",
          "id": 3,
          "image": "",
          "last_name": "",
          "status": true                
      }
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'
