# Zicochat

<!--  -->

----

## 1. [SOCKET] Zicochat / New Message

  <!--  -->



### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->

  ```javascript
  [SOCKET] channel: `???`
  ```

  * #### Method: `SOCKET`



### RESPONSE

  * #### Success:

    <!--  -->

    __Content:__

    ```json
    {
      "data": {
        "author": {
          "first_name": "",
          "last_name": ""
        },
        "content": "",
        "date": "",
        "status": true,
        "time": "10:12 AM",
        "type": "text || image || document",
        "userId": 3,
        "uuid": "110ec58a-a0f2-4ac4-8393-c866d813b8d1"
      }
    }
    ```



<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->



## 2. [SOCKET] Zicochat / Representatives New Status

  <!--  -->



### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->

  ```javascript
  [SOCKET] channel`connection`
  ```

  * #### Method: `SOCKET`



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "representative_id": 3,
        "status": false,
        "event":"user"
      }
    }
    ```


<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->

## 3. [SOCKET] Zicochat / New room

### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->

  ```javascript
  [SOCKET] channel`???`
  ```

  * #### Method: `SOCKET`



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "company_id":3,
        "representative_id": 3,
        "first_name": "",        
        "image": "",
        "last_name": "",
        "room_name":"skdfñzdfzjsdfjsdf-zsdfzsnf-zssf"
        "status": true
      },
      "notification": "new-room || user-connection"
    }
    ```


<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->
