# Zicochat

<!--  -->

----


## 2. [GET] Zicochat / Company

  <!--  -->


### PATH

  ```javascript
  [GET]
  `/api/${VERSION}/zicochat/company/${companyId}`
  ```

  * #### Method: `GET`

  * #### Params

    * __Required:__

      `companyId=[integer]` <br />



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "company": {
          "id": "",
          "image_200px": "",
          "name": "",
          "slug": "",
          "items": [
            {
              "name": "",
              "slug": ""
            }
          ]
        },
        "messages": [
          {
            "author": {
              "first_name": "",
              "last_name": ""
            },
            "content": "",
            "date": "",
            "status": true,
            "time": "10:12 AM",
            "type": "text || image || document",
            "user_id": 3,
            "uuid": "110ec58a-a0f2-4ac4-8393-c866d813b8d1"
          }
        ],
        "representative_id": 1,
        "room_name": "",
      }
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



### Notes:

  <!--  -->
