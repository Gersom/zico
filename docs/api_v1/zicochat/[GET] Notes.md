# ZICOCHAT

----

## 1. [GET] Zicochat / Notes

  Listado de NOTAS



### PATH

  ```javascript
  [GET]
  `/api/${VERSION}/zicochat/notes/${room_name}`
  ```

  * #### Method:   `GET`


### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "id": "",
          "content": "",
          "time": "20:15",
          "date": " 22 Junio"
        }
      ]
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



<!-- ### Notes: -->

  <!--  -->
