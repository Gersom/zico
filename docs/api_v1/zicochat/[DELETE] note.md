# Title

----

## 1. [DELETE] Note

### PATH

  ```javascript
  [DELETE]
  `/api/${VERSION}/zicochat/notes/${id}`
  ```

  * #### Method: `DELETE`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success":true      
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no existe esa nota <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado


### Notes:

Elimina la nota ...
