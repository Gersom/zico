# Zicochat
<!--  -->

----

## 1. [POST] New Message
  Enviar un nuevo mensaje

### PATH

  ```javascript  
  [POST]
  `/api/${VERSION}/zicochat/messages`
  ```

  * #### Method: `POST`



### REQUEST

  * #### Text:

  ```json
    {
      "message": "example",
      "roomName": "channel1",
      "type": "text",
      "uuid": "110ec58a-a0f2-4ac4-8393-c866d813b8d1"
    }
  ```

  OR

  * #### Image:

  ```json
    {
      "resource": "{Base64}",
      "roomName": "channel1",
      "type": "image",
      "uuid": "110ec58a-a0f2-4ac4-8393-c866d813b8d1"
    }
  ```

  OR

  * #### Document:

  ```json
    {
      "resource": "{Base64}",
      "roomName": "channel1",
      "type": "document",
      "uuid": "110ec58a-a0f2-4ac4-8393-c866d813b8d1"
    }
  ```



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success": true
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
