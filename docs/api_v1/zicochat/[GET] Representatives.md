# Zicochat

<!--  -->

----

## 1. [GET] Zicochat / Representatives

  Listado de representantes



### PATH

  ```javascript
  [GET]
  `/api/${VERSION}/zicochat/representatives`
  ```

  * #### Method:   `GET`



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        ...manyRepresentatives, {
          "company_id": 3,
          "company_name": "",
          "first_name": "",
          "id": 3,
          "image": "",
          "last_name": "",
          "position": "",
          "status": true
        }
      ]
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



### Notes:

  Lista de representantes de las compañias en la que el usuario ha entablado convesaión.
  position = cargo / sector position
  company_name = comercial,
