# Title
<!--  -->

----

## 1. [GET] Home
  <!--  -->

### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->
  ```javascript

  [POST]
  `/api/${VERSION}/start-connection`

  ```

  * #### Method:

    <!-- <_The request type_> -->

    `POST`

  *

### HEADERS
  * #### Content:

  ```json
    {
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "csrftoken": "{TOKEN}",
        "language": "es"
      }
    }
  ```


### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success": true,
      "code": "dsaefasdfakjsdfa"
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "success": false,
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  code es la ruta por la cual se envia parametros importantes del chat, es un codigo con fecha de caducidad
