# BRANCH OFFICE

----

## 1. [PUT] branch-office



### PATH

  ```javascript
  `/api/${VERSION}/branch-office/{id}`  
  ```

  * #### Method: `PUT`


### REQUEST

    * #### Content:

    ```json
      {
          "name":  "nueva sucursal",
          "address":  "av. lalala",
          "email":  "asjdfajsdf@gmail.com",
          "representative":  "lalalla",
          "website":  "www.google.com",
          "country_id":  1,
          "region_id":  1,
          "province_id":  1,
          "district_id":  1,
          "company_id": 1,
          "main": 1,
          "cellphones": [
            { "id": 1, "main": true, "value": "1234" },
            { "id": 0, "main": true, "value": "1234" }
        ],
        "phones": [
          { "id": 1, "main": true, "value": "1234" },
          { "id": 0, "main": true, "value": "1234" }
        ]      
      }
    ```

### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json

     {
         "success":true,
         "data":
            {
                "name":"nueva sucursal",
                "address":"av. lalala",
                "email":"asjdfajsdf@gmail.com",
                "representative":"lalalla",
                "website":"www.google.com",
                "country_id":1,
                "region_id":1,
                "province_id":1,
                "district_id":1,
                "company_id":1,
                "main":1,
                "updated_at":"2017-07-21 12:22:18",
                "created_at":"2017-07-21 12:22:18",
                "id":10
            }
     }

    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

notes:
    todo es requerido menos telefono y celular
