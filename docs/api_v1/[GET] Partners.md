# partners

----

## 1. [GET] partners



### PATH

  ```javascript
  `/api/${VERSION}/partners`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "image_url_200px": "",
          "image_url": "",
          "title": ""
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />



### Notes:

  No necesita logeado.
