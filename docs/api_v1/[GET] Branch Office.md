# PUBLIC

----

## 1. [GET] Testimonies



### PATH

  ```javascript
  `/api/${VERSION}/branch-office`
  `/api/${VERSION}/branch-office?zico=true`
  ```

  * #### Method: `GET`

  * #### Queries

    * __Optional:__

      `zico = [bolean]` <br />



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "cel": ["", "", ""],
          "company_id": 0,
          "country_image_url": "",
          "country_name": "",
          "email": "",
          "phone": ["", "", ""],
          "name": "",
          "id": 0
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

notes:
zico => true -> todas de la empresa zico
no found => envio todas las compañias  
