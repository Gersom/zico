# PUBLIC

----

## 1. [GET] Provinces



### PATH

  ```javascript
  `/api/${VERSION}/provinces?region=${0}`
  ```

  * #### Method: `GET`

  * #### Queries

    * __Optional:__

      `region = [integer]`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "id": 0,
          "name": ""
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />



### Notes:

  filtrar las provincias por el id de la region, si es requerido
