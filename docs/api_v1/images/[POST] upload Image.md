# Title
<!--  -->

----

## 1. [GET] Upload Image



### PATH

  ```javascript
  [POST]
  `/api/v1/upload-image`
  ```

  * #### Method: `POST`



### REQUEST

  <!--  -->

  * #### Content:

  ```json
    {
      "entity": "items|companies|posts",
      "entity_id": "4",
      "fileType": "png",
      "image": "zljfdhjxzdfxdfjdfxdfgxsdfgkmdfkgxfdg ... LOL",
    }
  ```

### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success" : true,      
      "data": {
       "image": "https://dl.dropboxuserc...",       
       "image_550px": "https://dl.dropboxuserc...",
       "image_200px": "https://dl.dropboxuserc...",       
       "id": 28
     }
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __Description:__ no se ha especifado algun campo <br />
    __content:__

    ```json
    {
      "success": false,
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  Se necesita tener el access token
