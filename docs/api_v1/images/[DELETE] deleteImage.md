# Title
<!--  -->

----

## 1. [GET] Home
  <!--  -->

### PATH

  <!-- <_The URL Structure (path only, no root url)_> -->
  ```javascript
  [DELETE]
  `/api/v1/delete-image`
  ```

  * #### Method:
    <!-- <_The request type_> -->
    `POST`


### REQUEST

  <!--  -->

  * #### Content:

  ```json
    {
      "index": "id de imagen",      
    }
  ```

### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "success": true,
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "success": false,
      "message": "Explicación del motivo ..."
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  <!-- This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->
