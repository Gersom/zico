# Title


----

## 1. [PUT] upload-image
  <!--  -->

### PATH

  ```javascript
  [PUT]
  `/api/${VERSION}/upload-image`
  ```

  * #### Method: `PUT`



### REQUEST

<!--  -->

* #### Content:

```json
    {
        "entity": "items|companies|posts",
        "entity_id": "4",
        "old_image_id": "4",
        "file_type": "png",
        "image": "zljfdhjxzdfxdfjdfxdfgxsdfgkmdfkgxfdg ... LOL",  
    }
```
<!-- name_camp = description,applications, references, params, attributes, sales, coin,price -->

### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
        "success" : true,      
        "data": {
         "image": "https://dl.dropboxuserc...",       
         "image_550px": "https://dl.dropboxuserc...",
         "image_200px": "https://dl.dropboxuserc...",       
         "id": 28
       }
    }
    ```


  * #### Error:
    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado

   OR

    __Code:__ 403 - FORBIDDEN <br />
    __Description:__ Si el logeado no es rol '1'


### Notes:

  Crea y elimina la antigua imagen.
