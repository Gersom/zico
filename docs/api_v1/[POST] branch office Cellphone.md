# BRANCH OFFICE CELLPHONE

----

## 1. [post] branch-office-cellphone


### PATH

  ```javascript
  `/api/${VERSION}/branch-office-cellphone`
  ```

  * #### Method: `PUT`


### REQUEST

    * #### Content:

    ```json
      {
          "id":1,
          "cellphones": [
            { "main": true, "value": "1234" },
            { "main": true, "value": "1234" }
          ]
      }
    ```

### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json

     {
         "success":true,         
     }

    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

notes:
    todo es requerido
