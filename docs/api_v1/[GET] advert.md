# advert

----

## 1. [GET] advert



### PATH

  ```javascript
  `/api/${VERSION}/advert`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "imageUrl": "",
        "url": ""
      }
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

notes:
    El ultimo anuncio creado ...
