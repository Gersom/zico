# BRANCH OFFICE

----

## 1. [PUT] branch-office



### PATH

  ```javascript
  `/api/${VERSION}/phone`
  ```

  * #### Method: `PUT`


### REQUEST

    * #### Content:

    ```json
      {
          "id":1        
      }
    ```

### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json

     {
         "success":true,         
     }

    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

notes:
    todo es requerido
