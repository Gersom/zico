# PUBLIC

----

## 1. [GET] Gains



### PATH

  ```javascript
  `/api/${VERSION}/gains`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "id": 0,
          "text": ""
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
