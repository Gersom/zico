# BRANCH OFFICE

----

## 1. [POST] branch-office



### PATH

  ```javascript
  `/api/${VERSION}/branch-office`  
  ```

  * #### Method: `POST`


### REQUEST

    * #### Content:

    ```json
      {
          "company_id":1,
          "country_id": 1,
          "name": "",
          "main":1,
          "region_id": 1,
          "province_id": 1,
          "district_id": 1,
          "address": "av. lalala",
          "representative": "lalalla",
          "phones": [
            { "main": true, "value": "1234" },
            { "main": false, "value": "1234" }
          ],
          "cellphones": [
            { "main": true, "value": "1234" },
            { "main": true, "value": "1234" }
          ],
          "email": "asjdfajsdf@gmail.com",
          "website": "www.google.com",
          "facebook": "fb.com/asd",
          "twitter": "tw.com/asd",    
      }
    ```

### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json

     {
         "success":true,
         "data":
            {
                "name":"nueva sucursal",
                "address":"av. lalala",
                "email":"asjdfajsdf@gmail.com",
                "representative":"lalalla",
                "website":"www.google.com",
                "country_id":1,
                "region_id":1,
                "province_id":1,
                "district_id":1,
                "company_id":1,
                "main":1,
                "updated_at":"2017-07-21 12:22:18",
                "created_at":"2017-07-21 12:22:18",
                "id":10,
                "cell":
                [
                    {
                        "id":32,
                        "cellphone":"1234",
                        "main":"1"
                    },
                    {
                        "id":33,
                        "cellphone":"4321",
                        "main":"0"
                    },
                    {
                        "id":34,
                        "cellphone":"1111",
                        "main":"0"
                        }
                ],
                "phone":
                [
                    {
                        "id":32,
                        "phone":"1234",
                        "main":"1"
                    },
                    {
                        "id":33,
                        "phone":"4321",
                        "main":"0"
                    },
                    {
                        "id":34,
                        "phone":"1111",
                        "main":"0"
                    }
                ]
            }
     }

    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

notes:
    todo es requerido menos telefono y celular
