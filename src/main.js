// import styles || importacion de estilos
require('@/styles/main.scss')
require('@/styles/main.styl')

// global axios configuration
import axios from 'axios'
import { camelizeKeys, decamelizeKeys } from 'humps'
axios.defaults.headers.common['Accept'] = 'application/json'
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.transformRequest = (data) => {
  return JSON.stringify(decamelizeKeys(data))
}
axios.defaults.transformResponse = (data) => {
  switch (typeof data) {
    case 'object':
      return camelizeKeys(data)
    case 'string':
      return camelizeKeys(JSON.parse(data))
  }
}

// global toastr configuration
window.toastr = require('toastr')
window.toastr.options = {
  closeButton: true,
  debug: false,
  extendedTimeOut: '1000',
  hideDuration: '1000',
  hideEasing: 'linear',
  hideMethod: 'fadeOut',
  newestOnTop: true,
  onclick: null,
  positionClass: 'toast-bottom-right',
  preventDuplicates: false,
  progressBar: true,
  showDuration: '300',
  showEasing: 'swing',
  showMethod: 'fadeIn',
  timeOut: '5000'
}

// init progressBar
window.pb = require('progressbar.js/dist/progressbar.js')

// init jquery
import Jquery from 'jquery'
window.$ = Jquery

// init socket.io
window.io = require('socket.io-client/dist/socket.io')

// Generate a v4 UUID (random)
window.uuidV4 = require('uuid/v4')

// init swiper
import Swiper from 'swiper'
window.Swiper = Swiper

// Mounted Vue app || Aplicación Vue montada
import { app } from '@/app'
app.$mount('#root')
