export function languages (code) {
  switch (code) {
    case 'es':
      return 'español'
    case 'en':
      return 'english'
    case 'ES':
      return 'español'
    case 'EN':
      return 'english'
    default:
      return 'not-found'
  }
}
