// si el token no existe
export function notToken (dis) {
  if (!dis.tokenState) {
    dis.$router.push({ name: '404' })
  }
}
