import AllNotFoundScreen from './components/Screen'

export default {
  path: '*',
  name: 'AllNotFound',
  component: AllNotFoundScreen
}
