import FourHundredFourScreen from './components/Screen'

export default {
  path: '/404',
  name: '404',
  component: FourHundredFourScreen
}
