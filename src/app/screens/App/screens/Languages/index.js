import LanguagesScreen from './components/Screen'
import Default from './screens/Default'

import Blog from './screens/Blog'
import Companies from './screens/Companies'
import Offers from './screens/Offers'
import Login from './screens/Login'
import News from './screens/News'
import Recover from './screens/Recover'
import UpdateProfile from './screens/UpdateProfile'
import ZicoChat from './screens/ZicoChat'

export default {
  children: [
    Default,        // path: ''

    Blog,           // path: 'blog'
    Companies,      // path: 'directory'
    Offers,         // path: 'offers'
    Login,          // path: 'login'
    News,           // path: 'n'
    Recover,        // path: 'recover'
    UpdateProfile,  // path: 'update-profile'
    ZicoChat        // path: 'zicochat'
  ],
  path: '/:langCode',
  component: LanguagesScreen
}
