import CreateOfferScreen from './components/Screen'

export default {
  component: CreateOfferScreen,
  name: 'CreateOffer',
  path: 'create-offer'
}
