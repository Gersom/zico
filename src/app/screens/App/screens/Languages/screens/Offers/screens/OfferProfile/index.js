import OfferProfileScreen from './components/Screen'

export default {
  component: OfferProfileScreen,
  name: 'OfferProfile',
  path: ':offerSlug'
}
