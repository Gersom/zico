import OffersScreen from './components/Screen'
import Default from './screens/Default'

import CreateOffer from './screens/CreateOffer'
import OfferProfile from './screens/OfferProfile'

export default {
  children: [
    Default,        // path: ''

    CreateOffer,    // path: 'create-offer'
    OfferProfile    // path: ':offer_slug'
  ],
  component: OffersScreen,
  path: 'offers'
}
