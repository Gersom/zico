import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'ZicoChat',
  path: 'zicochat'
}
