import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'CompanyProfile',
  path: ':companySlug'
}
