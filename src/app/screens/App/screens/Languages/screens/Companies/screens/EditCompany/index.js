import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'EditCompany',
  path: ':companySlug/edit'
}
