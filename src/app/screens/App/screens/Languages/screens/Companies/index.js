import OffersScreen from './components/Screen'
import Default from './screens/Default'

import CompanyProfile from './screens/CompanyProfile'
import EditCompany from './screens/EditCompany'

export default {
  children: [
    Default,         // path: ''
    EditCompany,     // path: ':companySlug/edit'
    CompanyProfile   // path: ':companySlug'
  ],
  component: OffersScreen,
  path: 'directory'
}
