import UpdateProfileScreen from './components/Screen'

export default {
  component: UpdateProfileScreen,
  name: 'UpdateProfile',
  path: 'update-profile'
}
