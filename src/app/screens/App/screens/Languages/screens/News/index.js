import Screen from './components/Screen'
import Default from './screens/Default'

import CreateNews from './screens/CreateNews'
import NoticeProfile from './screens/NoticeProfile'
import UpdateNotice from './screens/UpdateNotice'

export default {
  children: [
    Default,        // path: ''

    CreateNews,     // path: 'create-news'
    NoticeProfile,  // path: ':noticeSlug'
    UpdateNotice    // path: ':noticeSlug/edit'
  ],
  component: Screen,
  path: 'n'
}
