import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'NoticeProfile',
  path: ':noticeSlug'
}
