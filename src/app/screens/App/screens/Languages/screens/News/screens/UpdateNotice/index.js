import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'UpdateNotice',
  path: ':noticeSlug/edit'
}
