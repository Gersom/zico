import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'CreateNews',
  path: 'create-news'
}
