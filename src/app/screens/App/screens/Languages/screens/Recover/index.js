import RecoverScreen from './components/Screen'

export default {
  component: RecoverScreen,
  name: 'Recover',
  path: 'recover'
}
