import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'BlogProfile',
  path: ':noticeSlug'
}
