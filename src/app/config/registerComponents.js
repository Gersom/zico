import GerButton from 'components/GerButton'
import GerLink from 'components/GerLink'
import GerLoading from 'components/GerLoading'
import GerUserConfig from 'components/GerUserConfig'

// import { swiper, swiperSlide } from 'vue-awesome-swiper'
// import Datepicker from 'vuejs-datepicker'
import { VueEditor } from 'vue2-editor'

import AppHeader from 'App/shared/components/AppHeader'
import AppNav from 'App/shared/components/AppNav'
import AppFooter from 'App/shared/components/AppFooter'
import Company from 'App/shared/components/Company'
import ItemList from 'App/shared/components/ItemList'
import MenuAside from 'App/shared/components/MenuAside'
import Notice from 'App/shared/components/Notice'
import Offer from 'App/shared/components/Offer'
import OfferInfo from 'App/shared/components/OfferInfo'
import OfferInfoUpdate from 'App/shared/components/OfferInfoUpdate'
import ProcessPercentage from 'App/shared/components/ProcessPercentage'
import ProfileLink from 'App/shared/components/ProfileLink'
import SectionsWrapper from 'App/shared/components/SectionsWrapper'
import TabsCompact from 'App/shared/components/TabsCompact'
import TabsCompactUpdate from 'App/shared/components/TabsCompactUpdate'
import UpdateButton from 'components/UpdateButton'
import UserModal from 'App/shared/components/UserModal'
import UserProfile from 'App/shared/components/UserProfile'

export default function registerComponents (Vue) {
  Vue.component('app-button', GerButton)
  Vue.component('app-link', GerLink)
  Vue.component('app-loading', GerLoading)
  Vue.component('app-user-config', GerUserConfig)

  // Vue.component('swiper-slide', swiperSlide)
  // Vue.component('swiper', swiper)
  // Vue.component('date-picker', Datepicker)
  Vue.component('vue-editor', VueEditor)

  Vue.component('app-header', AppHeader)
  Vue.component('app-nav', AppNav)
  Vue.component('app-footer', AppFooter)
  Vue.component('company', Company)
  Vue.component('item-list', ItemList)
  Vue.component('menu-aside', MenuAside)
  Vue.component('notice', Notice)
  Vue.component('offer-info-update', OfferInfoUpdate)
  Vue.component('offer-info', OfferInfo)
  Vue.component('offer', Offer)
  Vue.component('process-percentage', ProcessPercentage)
  Vue.component('profile-link', ProfileLink)
  Vue.component('sections-wrapper', SectionsWrapper)
  Vue.component('tabs-compact-update', TabsCompactUpdate)
  Vue.component('tabs-compact', TabsCompact)
  Vue.component('update-button', UpdateButton)
  Vue.component('user-modal', UserModal)
  Vue.component('user-profile', UserProfile)
}
