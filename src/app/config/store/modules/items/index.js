import * as types from './types'
import * as defaults from './defaults'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  all: [],
  viewedItems: [],
  lastSeenItem: defaults.LAST_SEEN_ITEM,
  searchText: ''
}

const mutations = {
  [types.BEGIN_ITEMS] (state, items) {
    state.all = items
  },
  [types.ADD_NEW_PHOTO] (state, element) {
    state.lastSeenItem.image550px.push(element)
  },
  [types.BEGIN_SPECIFIC_ITEM] (state, item) {
    if (item === false) {
      state.lastSeenItem = defaults.LAST_SEEN_ITEM
    } else {
      state.lastSeenItem = item
    }
  },
  [types.SAVE_TEXT_SEARCH] (state, text) {
    state.searchText = text
  },
  [types.DEFAULT_TEXT_SEARCH] (state, text) {
    state.searchText = ''
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
