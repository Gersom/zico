export const ALL = {
  companyName: '',
  companySlug: '',
  datePublish: '2017-03-08',
  id: 0,
  image_200px: '',
  image_550px: '',
  image: '',
  name: '',
  sectorId: 0
}

export const LAST_SEEN_ITEM = {
  applications: '',
  attributes: '',
  coin: '',
  description: '',
  id: 0,
  image_550px: [''],
  name: '',
  params: '',
  price: '',
  references: '',
  sale: '',
  sector: '',
  company: {
    id: '',
    image_200px: '',
    name: '',
    slug: ''
  }
}
