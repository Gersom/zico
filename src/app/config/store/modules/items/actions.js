// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const beginItems = ({commit, dispatch, state, rootState}) => {
    commit(types.DEFAULT_TEXT_SEARCH)
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/items`,
      params: {
        region_id: rootState.regions.select.id,
        sector_id: rootState.sectors.select.id
      },
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_ITEMS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      if (response.status === 404) {
        if (response.data.data) {
          commit(types.BEGIN_ITEMS, response.data.data)
        }
      }
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const beginItemsSeach = ({commit, rootState}, query) => {
    commit(types.SAVE_TEXT_SEARCH, query)
    commit('SECTOR_SELECTED_DEFAULT')
    commit('REGION_SELECTED_DEFAULT')
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/items`,
      params: { q: query },
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_ITEMS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      let noResult = false
      if (response.status === 404) {
        if (response.data.data) {
          if (response.data.data.length === 0) {
            noResult = true
          }
        }
      }
      if (noResult) {
        window.toastr.warning('Lo sentimos no encontramos ofertas relacionadas.', 'ZICOMARKET')
      } else {
        window.toastr.error('Lo sentimos ocurrió un error al buscar ofertas.', 'ZICOMARKET')
      }
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const beginSpecificItem = ({commit, rootState}, offerSlug) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/items/${offerSlug}`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_SPECIFIC_ITEM, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.BEGIN_SPECIFIC_ITEM, false)
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const withoutFilters = ({commit, rootState}) => {
    commit(types.DEFAULT_TEXT_SEARCH)
    commit('SECTOR_SELECTED_DEFAULT')
    commit('REGION_SELECTED_DEFAULT')
  }

  export const addNewPhoto = ({commit}, element) => {
    commit(types.ADD_NEW_PHOTO, element)
  }
