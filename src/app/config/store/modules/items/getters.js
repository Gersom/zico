export const itemsAll = (state, getters, rootState) => {
  return state.all
}

export const currentItem = (state, getters, rootState) => {
  return state.lastSeenItem
}

export const textSearch = (state) => {
  return state.searchText
}
