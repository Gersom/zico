// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const requestCountries = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/countries/`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.FILL_COUNTRIES, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestCountriesVisible = ({commit, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/countries/`,
      params: { visible: true },
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.FILL_COUNTRIES_VISIBLE, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }
