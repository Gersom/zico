// mutations
export const FILL_COUNTRIES = 'FILL_COUNTRIES'
export const FILL_COUNTRIES_VISIBLE = 'FILL_COUNTRIES_VISIBLE'
export const COUNTRY_SELECTION = 'COUNTRY_SELECTION'
export const COUNTRY_SELECTED_DEFAULT = 'COUNTRY_SELECTED_DEFAULT'
