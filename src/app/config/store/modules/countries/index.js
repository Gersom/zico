import * as actions from './actions'
import * as getters from './getters'
import * as types from './types'

const state = {
  all: [],
  visible: [],
  select: { slug: '' }
}

const mutations = {
  [types.FILL_COUNTRIES] (state, elements) {
    state.all = elements
  },
  [types.FILL_COUNTRIES_VISIBLE] (state, elements) {
    state.visible = elements
  },
  [types.COUNTRY_SELECTION] (state, element) {
    state.select = element
  },
  [types.COUNTRY_SELECTED_DEFAULT] (state) {
    state.select = { slug: '' }
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
