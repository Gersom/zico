// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const beginRegions = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/regions/`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_REGIONS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const beginRegionsId = ({commit, rootState}, id) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/regions?country=${id}`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_REGIONS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const regionSelection = ({ commit, state }, region) => {
    // commit('SECTOR_SELECTED_DEFAULT')
    commit(types.REGION_SELECTION, region)
  }
