import * as actions from './actions'
import * as getters from './getters'
import * as types from './types'

const state = {
  all: [
  // {
  //   'countryId': '',
  //   'countryName': '',
  //   'id': 0,
  //   'image': '',
  //   'name': '',
  //   'slug': ''
  // }
  ],
  select: { slug: '' }
}

const mutations = {
  [types.BEGIN_REGIONS] (state, regions) {
    state.all = regions
  },
  [types.REGION_SELECTION] (state, region) {
    state.select = region
  },
  [types.REGION_SELECTED_DEFAULT] (state) {
    state.select = {slug: ''}
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
