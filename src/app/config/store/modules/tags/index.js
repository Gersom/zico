import * as actions from './actions'
import * as getters from './getters'
import * as types from './types'

const state = {
  all: []
}

const mutations = {
  [types.TAGS_FILL] (state, elements) {
    state.all = elements
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
