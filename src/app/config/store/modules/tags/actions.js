// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const requestTags = ({commit, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/tags`,
      headers: {
        'language': rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.TAGS_FILL, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }
