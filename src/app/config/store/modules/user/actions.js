// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const beginUserProfile = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/user-profile/`,
      headers: {
        'language': rootState.languages.select.code,
        'Authorization': 'Bearer ' + rootState.oauth.token.access
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_USER_PROFILE, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }
