import * as actions from './actions'
import * as getters from './getters'
import * as types from './types'

const state = {
  profile: {
    // firstName: 'Jonathan',
    // image200px: 'https://d30y9cdsu7xlg0.cloudfront.net/png/36874-200.png',
    // lastName: 'Conde',
    // role: 1
    company: {
      name: '',
      slug: 'my-company'
    }
  }
}

const mutations = {
  [types.BEGIN_USER_PROFILE] (state, DATA) {
    state.profile = DATA
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
