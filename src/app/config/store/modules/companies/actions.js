// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONSs
  export const beginCompanies = ({commit, rootState}) => {
    // let regionId = rootState.regions.select ? rootState.regions.select.id : 0
    // let sectorId = rootState.sectors.select ? rootState.sectors.select.id : 0
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/companies`,
      // params: {
      //   region_id: regionId,
      //   sector_id: sectorId
      // },
      headers: {
        language: rootState.languages.select.code,
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + rootState.oauth.token.access
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_COMPANIES, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => { commit(types.AJAX_CALL_ERROR) })
  }

  export const beginSpecificCompany = ({commit, rootState}, code) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/companies/${rootState.route.params.companySlug}`,
      headers: {
        language: code || rootState.languages.select.code,
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + rootState.oauth.token.access
      }
    })
    .then(({ data }) => {
      let dataToSave = data.data
      dataToSave.items.find((element, index) => {
        element.companyName = dataToSave.name
      })
      commit(types.BEGIN_SPECIFIC_COMPANY, dataToSave)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.BEGIN_SPECIFIC_COMPANY, false)
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestCompanyMe = ({commit, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/my-company`,
      headers: {
        language: rootState.languages.select.code,
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + rootState.oauth.token.access
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_MY_COMPANY, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const updateImageCompany = ({commit, rootState}, parameter) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'put',
      url: `${window.API_URL}/update-logo-company`,
      headers: {
        language: rootState.languages.select.code,
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + rootState.oauth.token.access
      },
      data: {
        companyId: rootState.companies.lastSeenCompany.id,
        fileType: parameter.fileType,
        image: parameter.image
      }
    })
    .then(({ data }) => {
      commit(types.UPDATE_IMAGE_CURRENT_COMPANY, parameter.image)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const companiesFilteredBySector = ({commit}, element) => {
    commit(types.COMPANIES_FILTERED_BY_SECTOR, element.id)
  }
  export const companiesFilteredByCountry = ({commit}, element) => {
    commit(types.COMPANIES_FILTERED_BY_COUNTRY, element.id)
  }
  export const companiesFilteredByName = ({commit}, text) => {
    commit(types.COMPANIES_FILTERED_BY_NAME, text)
  }
  export const companiesNotFilters = ({commit}, element) => {
    commit(types.COMPANIES_NOT_FILTERS)
  }
