export const companiesAll = (state) => {
  return state.all
}
export const filteredCompanies = (state) => {
  return state.filteredCompanies
}
export const companiesFilters = (state) => {
  return state.filters
}

export const cantUpdatingCurrentCompany = (state) => {
  return state.cantUpdatingCurrentCompany
}

export const currentCompany = (state) => {
  return state.lastSeenCompany
}

export const myCompany = (state) => {
  return state.myCompany
}
