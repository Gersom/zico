import * as types from './types'
import * as defaults from './defaults'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  all: [],

  filteredCompanies: [],
  filters: {
    searchName: false,
    sectorId: false,
    countryId: false
  },

  lastSeenCompany: defaults.COMPANY,
  cantUpdatingCurrentCompany: 0,
  myCompany: {}
}

const mutations = {
  [types.BEGIN_COMPANIES] (state, elements) {
    state.all = elements
    state.filteredCompanies = elements
  },
  [types.COMPANIES_NOT_FILTERS] (state) {
    state.filteredCompanies = state.all
    state.filters = {
      searchName: false,
      sectorId: false,
      countryId: false
    }
  },
  [types.COMPANIES_FILTERED_BY_NAME] (state, text) {
    let record = state.all.filter((ele) => {
      let original = ele.name.toLowerCase()
      return original.search(text.toLowerCase()) > -1
    })
    if (record) {
      state.filteredCompanies = record
      state.filters = {
        searchName: text,
        sectorId: false,
        countryId: false
      }
    }
  },
  [types.COMPANIES_FILTERED_BY_SECTOR] (state, id) {
    let record = state.all.filter(ele => ele.sectorId === id)
    if (record) {
      state.filteredCompanies = record
      state.filters.sectorId = id
    }

    state.filters.searchName = false
    if (state.filters.countryId !== false) {
      record = state.filteredCompanies.filter((ele) => {
        return ele.countryId === state.filters.countryId
      })
      if (record) {
        state.filteredCompanies = record
      }
    }
  },
  [types.COMPANIES_FILTERED_BY_COUNTRY] (state, id) {
    let record = state.all.filter(ele => ele.countryId === id)
    if (record) {
      state.filteredCompanies = record
      state.filters.countryId = id
    }

    state.filters.searchName = false
    if (state.filters.sectorId !== false) {
      record = state.filteredCompanies.filter((ele) => {
        return ele.sectorId === state.filters.sectorId
      })
      if (record) {
        state.filteredCompanies = record
      }
    }
  },
  [types.BEGIN_SPECIFIC_COMPANY] (state, element) {
    if (element) {
      state.lastSeenCompany = element
    } else if (element === false) {
      state.lastSeenCompany = defaults.LAST_SEEN_COMPANY
    } else {
      console.error('Vuex error\n=> module: companies\nmutation: BEGIN_SPECIFIC_COMPANY')
    }
  },
  [types.BEGIN_MY_COMPANY] (state, element) {
    if (element) {
      state.myCompany = element
    } else {
      console.error('Vuex error\n=> module: companies\nmutation: BEGIN_MY_COMPANY')
    }
  },
  [types.UPDATE_IMAGE_CURRENT_COMPANY] (state, element) {
    state.lastSeenCompany.image200px = element
    state.cantUpdatingCurrentCompany += 1
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
