export const COMPANY = {
  id: 0,
  image200px: '',
  name: '',
  sectorId: 0,
  slug: ''
}

export const LAST_SEEN_COMPANY = {
  applications: '',
  attributes: '',
  coin: '',
  description: '',
  id: 0,
  image_550px: [''],
  name: '',
  params: '',
  price: '',
  references: '',
  sale: '',
  sector: '',
  company: {
    id: '',
    image_200px: '',
    name: '',
    slug: ''
  }
}
