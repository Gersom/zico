// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const requestNews = ({commit, rootState}) => {
    let axiosConfig = {
      method: 'get',
      url: `${window.API_URL}/news`,
      headers: {
        'Content-Type': 'application/json',
        language: rootState.languages.select.code
      }
    }

    if (rootState.route.query.tag) {
      axiosConfig.params = {
        tag: rootState.route.query.tag
      }
    }

    commit(types.BEGIN_AJAX_CALL)
    axios(axiosConfig)
    .then(({ data }) => {
      commit(types.FILL_NEWS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestLastNotice = ({commit, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/news/last`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.FILL_LAST_NOTICE, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestNoticeProfile = ({commit, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/news/${rootState.route.params.noticeSlug}`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.FILL_NOTICE_PROFILE, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestPartners = ({commit, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/partners`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.FILL_PARTNERS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestAdvert = ({commit, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/advert`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.FILL_ADVERT, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.AJAX_CALL_ERROR)
    })
  }
