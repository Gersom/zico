// mutations
export const FILL_NEWS = 'FILL_NEWS'
export const FILL_LAST_NOTICE = 'FILL_LAST_NOTICE'
export const FILL_NOTICE_PROFILE = 'FILL_NOTICE_PROFILE'
export const FILL_PARTNERS = 'FILL_PARTNERS'
export const FILL_ADVERT = 'FILL_ADVERT'
