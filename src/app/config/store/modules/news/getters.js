export const newsAll = (state, getters, rootState) => {
  return state.all
}

export const lastNotice = (state, getters, rootState) => {
  return state.last
}

export const noticeProfile = (state, getters, rootState) => {
  return state.profile
}

export const partners = (state) => {
  return state.partners
}

export const advert = (state) => {
  return state.advert
}
