import * as actions from './actions'
import * as getters from './getters'
import * as types from './types'

let select = {code: 'ES', id: 0, name: 'Español'}
if (window.LANGUAGES) {
  if (window.LANGUAGES.length > 0) {
    select = window.LANGUAGES[0]
  }
}
const state = {
  all: window.LANGUAGES,
  select: select
}

const mutations = {
  [types.BEGIN_LANGUAGES] (state, languages) {
    state.all = languages
  },
  [types.SELECTING_FIRST_LANGUAGE] (state) {
    state.select = state.all[0]
  },
  [types.SELECT_LANGUAGE] (state, language) {
    state.select = language
  },
  [types.SELECT_LANGUAGE_START] (state, code) {
    let record = state.all.find((element) => {
      return element.code.toUpperCase() === code.toUpperCase()
    })
    if (record) {
      state.select = record
    }
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
