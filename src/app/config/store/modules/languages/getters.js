export const languagesAll = (state) => {
  return state.all
}

export const selectedLanguage = (state) => {
  for (let i in state.select) { return state.select }
  return false
}

export const selectedLanguageJson = (state) => {
  return state.select
}

export const idSelectedLanguage = (state) => {
  return state.select.id
}
