// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const beginLanguages = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/languages/`
    })
    .then(({ data }) => {
      commit(types.BEGIN_LANGUAGES, data.data)
      commit(types.SELECTING_FIRST_LANGUAGE)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const selectLanguages = ({ commit }, language) => {
    commit(types.SELECT_LANGUAGE, language)
  }

  export const startSelectLanguages = ({ commit, rootState }) => {
    commit(types.SELECT_LANGUAGE_START, rootState.route.params.langCode)
  }
