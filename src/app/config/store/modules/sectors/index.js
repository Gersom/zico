import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  all: [
  // {
  //   'id': 0,
  //   'image': '',
  //   'name': '',
  //   'slug': ''
  // }
  ],
  select: {
    slug: ''
  }
}

const mutations = {
  [types.BEGIN_SECTORS] (state, sectors) {
    state.all = sectors
  },
  [types.SECTOR_SELECTION] (state, sector) {
    state.select = sector
  },
  [types.SECTOR_SELECTED_DEFAULT] (state) {
    state.select = {slug: ''}
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
