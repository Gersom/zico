// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const beginSectors = ({ commit, rootState }) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/sectors/`,
      headers: {
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_SECTORS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const sectorSelection = ({ commit, state }, sector) => {
    // let record = state.all.find((item) => item === sector)
    // if (record) {
    //   commit(types.SECTOR_SELECTION, record)
    // }
    // commit('REGION_SELECTED_DEFAULT')
    commit(types.SECTOR_SELECTION, sector)
  }

  export const sectorSelectedDefault = ({ commit, state }) => {
    commit(types.SECTOR_SELECTED_DEFAULT)
  }
