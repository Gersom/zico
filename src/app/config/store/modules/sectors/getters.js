export const sectorsAll = (state, getters, rootState) => {
  let temporal = state.all
  temporal.forEach((element, index, array) => {
    let shortName = element.name
    if (element.name.length > 15) {
      shortName = element.name.substr(0, 15) + '...'
    }
    temporal[index]['shortName'] = shortName
  })
  return temporal
}

export const selectedSector = (state) => {
  for (let i in state.select) { return state.select }
  return false
}
