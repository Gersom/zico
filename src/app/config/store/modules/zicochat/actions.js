// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const createdRoom = ({ commit, rootState }, elementId) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'post',
      url: `${window.API_URL}/zicochat/room/${elementId}`,
      data: {},
      headers: {
        Authorization: `Bearer ${rootState.oauth.token.access}`,
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.ADD_REPRESENTATIVES, data.data)
      commit(types.CURRENT_REPRESENTATIVES, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const beginRepresentatives = ({ commit, rootState }) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/zicochat/representatives/`,
      headers: {
        Authorization: `Bearer ${rootState.oauth.token.access}`,
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.BEGIN_REPRESENTATIVES, data.data)
      commit(types.GET_REPRESENTATIVES_TRUE)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const fillChat = ({ commit, state, rootState }, companyId) => {
    commit(types.BEGIN_AJAX_CALL)
    axios({
      method: 'get',
      url: `${window.API_URL}/zicochat/company/${companyId}`,
      headers: {
        Authorization: `Bearer ${rootState.oauth.token.access}`,
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      let dataTmp = data.data
      dataTmp.messages.reverse()
      commit(types.BEGIN_MESSAGES, dataTmp.messages)
      commit(types.ROOMS_UPDATE, dataTmp)
      commit(types.GET_REPRESENTATIVES_FALSE)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const submitNewMessage = ({ commit, state, rootState }, message) => {
    let meses = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
    let f = new Date()
    let jsonToSave = {
      author: {
        firstName: 'Yo',
        lastName: ''
      },
      content: message,
      date: `${f.getDate()} ${meses[f.getMonth()]}`,
      status: false,
      time: `${f.getHours()}:${f.getMinutes()}:${f.getSeconds()}`,
      type: 'text',
      userId: rootState.user.profile.userId,
      uuid: window.uuidV4()
    }
    commit(types.NEW_MESSAGES, jsonToSave)
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'post',
      url: `${window.API_URL}/zicochat/message`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${rootState.oauth.token.access}`,
        language: rootState.languages.select.code
      },
      data: {
        message: message,
        roomName: state.currentRoom.roomName,
        type: 'text',
        uuid: jsonToSave.uuid
      }
    })
    .then(() => { commit(types.AJAX_CALL_SUCCESS) })
    .catch(() => { commit(types.AJAX_CALL_ERROR) })
  }

  export const addNewMessage = ({ commit, state, rootState }, message) => {
    if (message.userId) {
      if (`${message.userId}` === `${rootState.user.profile.userId}`) {
        let tmpIndex
        for (let i = (state.messages.length - 1); i > -1; i--) {
          if (state.messages[i].uuid === message.uuid) {
            tmpIndex = i
            break
          }
        }
        commit(types.UPDATE_MESSAGE, {index: tmpIndex, data: true})
      } else {
        commit(types.NEW_MESSAGES, message)
      }
    } else {
      console.error('Vuex: Error Action\n=> Function: fillNewMessage\n=> Line: 103\n=> Error: \'userId\' not found')
    }
  }

  export const referenceCompany = ({ commit }, element) => {
    commit(types.FILL_REFERENCE_COMPANY, parseInt(element))
  }

  export const changeRepresentative = ({ commit }, rep) => {
    commit(types.CURRENT_REPRESENTATIVES, rep)
  }

  // ---------------------------------------
  // STATUS REPRESENTATIVES CONNECTION
  // ---------------------------------------
  export const trueStateRepresentative = ({commit}, id) => {
    commit(types.TRUE_STATE_REPRESENTATIVE, id)
  }
  export const falseStateRepresentative = ({commit}, id) => {
    commit(types.FALSE_STATE_REPRESENTATIVE, id)
  }

  // ---------------------------------------
  // NOTES
  // ---------------------------------------
  export const requestNotes = ({commit, rootState}, roomName) => {
    commit(types.BEGIN_AJAX_CALL)
    axios({
      method: 'get',
      url: `${window.API_URL}/zicochat/notes/${roomName}`,
      headers: {
        Authorization: `Bearer ${rootState.oauth.token.access}`,
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.NOTES_FILL, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const updateNote = ({commit, rootState}, element) => {
    commit(types.BEGIN_AJAX_CALL)
    axios({
      method: 'put',
      url: `${window.API_URL}/zicochat/notes`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${rootState.oauth.token.access}`,
        language: rootState.languages.select.code
      },
      data: element
    })
    .then(({ data }) => {
      commit(types.NOTES_UPDATE, data.data)
      commit(types.AJAX_CALL_SUCCESS)
      window.toastr.success('La nota ah sido editada', 'ZICOMARKET')
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
      window.toastr.error('Lo sentimos, ocurrió un error al editar la nota', 'ZICOMARKET')
    })
  }

  export const createNote = ({commit, rootState}, element) => {
    commit(types.BEGIN_AJAX_CALL)
    axios({
      method: 'post',
      url: `${window.API_URL}/zicochat/notes`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${rootState.oauth.token.access}`,
        language: rootState.languages.select.code
      },
      data: element
    })
    .then(({ data }) => {
      commit(types.NOTES_CREATED, data.data)
      commit(types.AJAX_CALL_SUCCESS)
      window.toastr.success('Enhorabuena nueva nota creada', 'ZICOMARKET')
    })
    .catch(() => {
      window.toastr.error('Lo sentimos, ocurrió un error al crear una nueva nota', 'ZICOMARKET')
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const deleteNote = ({commit, rootState}, id) => {
    commit(types.BEGIN_AJAX_CALL)
    axios({
      method: 'delete',
      url: `${window.API_URL}/zicochat/notes/${id}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${rootState.oauth.token.access}`,
        language: rootState.languages.select.code
      }
    })
    .then(({ data }) => {
      commit(types.NOTES_DELETE, id)
      commit(types.AJAX_CALL_SUCCESS)
      window.toastr.success('Nota eliminada', 'ZICOMARKET')
    })
    .catch(() => {
      window.toastr.error('Lo sentimos, ocurrió un error al eliminar la nota', 'ZICOMARKET')
      commit(types.AJAX_CALL_ERROR)
    })
  }
