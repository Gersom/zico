import * as defaults from './defaults'

// ---------------------------------------
// REPRESENTATIVES
// ---------------------------------------
export const zicochGetRepresentatives = (state, getters) => {
  return state.getRepresentatives
}

export const zicochatRepresentativesAll = (state, getters, rootState) => {
  return state.representatives
}
export const zicochatLastRepresentative = (state, getters, rootState) => {
  return state.representatives[0]
}
export const zicochatCurrentRepresentative = (state, getters, rootState) => {
  for (var i in state.currentRepresentative) {
    return state.currentRepresentative
  }
  return defaults.RESPRESENTATIVE
}

// ---------------------------------------
// REFERENCE
// ---------------------------------------
export const zicochatReferenceCompanyId = (state, getters) => {
  return state.referenceCompanyId
}
export const zicochatReferenceCompany = (state, getters) => {
  if (state.referenceCompanyId === false) {
    return false
  } else {
    return true
  }
}
export const zicochatExistsReferenceInRepresentatives = (state, getters) => {
  const record = state.representatives.find((element) => {
    return parseInt(element.companyId) === parseInt(state.referenceCompanyId)
  })
  if (record) {
    return true
  } else {
    return false
  }
}

// ---------------------------------------
// MESSAGES
// ---------------------------------------
export const zicochatMessagesAll = (state, getters, rootState) => {
  return state.messages
}

// ---------------------------------------
// ROOMS
// ---------------------------------------
export const zicochatThereAreRooms = (state, getters) => {
  return state.rooms.length > 0
}
export const zicochatCurrentRoom = (state, getters) => {
  for (var i in state.currentRoom) { return state.currentRoom }
  return defaults.ROOM
}

// ---------------------------------------
// NOTES
// ---------------------------------------
export const zicochatNotesAll = (state, getters) => {
  return state.notes
}
