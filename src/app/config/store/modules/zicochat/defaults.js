export const RESPRESENTATIVE = {
  companyId: 0,
  firstname: '',
  image: '',
  lastname: '',
  status: true,
  userId: 0
}

export const MESSAGE = {
  content: '',
  date: '',
  room: '',
  time: '10:12 AM',
  userId: 1
}

export const ROOM = {
  company: {
    id: '',
    image_200px: '',
    name: '',
    slug: '',
    items: [
      {
        name: '',
        slug: ''
      }
    ]
  },
  messages: [
    {
      content: '',
      date: '',
      time: '10:12 AM',
      userId: 1
    }
  ],
  representativeId: 1,
  roomName: ''
}
