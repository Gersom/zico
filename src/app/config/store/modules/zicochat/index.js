import * as actions from './actions'
// import * as defaults from './defaults'
import * as getters from './getters'
import * as types from './types'

const state = {
  messages: [],
  referenceCompanyId: false,
  getRepresentatives: false,
  representatives: [],
  currentRepresentative: false, // => {}
  currentRoom: {},
  rooms: [],

  notes: []
}

const mutations = {
  // ---------------------------------------
  // REPRESENTATIVES
  // ---------------------------------------
  [types.BEGIN_REPRESENTATIVES] (state, reps) {
    state.representatives = reps
  },
  [types.CURRENT_REPRESENTATIVES] (state, rep) {
    state.currentRepresentative = rep
  },
  [types.ADD_REPRESENTATIVES] (state, rep) {
    const exists = state.representatives.find((ele) => {
      return parseInt(ele.id) === parseInt(rep.id)
    })
    if (!exists) {
      state.representatives.push(rep)
    }
  },
  [types.GET_REPRESENTATIVES_TRUE] (state) {
    state.getRepresentatives = true
  },
  [types.GET_REPRESENTATIVES_FALSE] (state, rep) {
    state.getRepresentatives = false
  },
  [types.TRUE_STATE_REPRESENTATIVE] (state, id) {
    state.representatives.find((element) => {
      if (parseInt(element.id) === parseInt(id)) {
        element.status = true
      }
    })
  },
  [types.FALSE_STATE_REPRESENTATIVE] (state, id) {
    state.representatives.find((element) => {
      if (parseInt(element.id) === parseInt(id)) {
        element.status = false
      }
    })
  },

  // ---------------------------------------
  // MESSAGES
  // ---------------------------------------
  [types.BEGIN_MESSAGES] (state, msgs) {
    state.messages = msgs
  },
  [types.NEW_MESSAGES] (state, msg) {
    state.messages.push(msg)
  },
  [types.UPDATE_MESSAGE] (state, objTmp) {
    state.messages[objTmp.index].status = objTmp.data
    setTimeout(() => {
      document.getElementById('c-chat__messages').scrollTop = 999999999
    }, 5)
  },

  // ---------------------------------------
  // REFERNCE
  // ---------------------------------------
  [types.FILL_REFERENCE_COMPANY] (state, id) {
    state.referenceCompanyId = id
  },

  // ---------------------------------------
  // ROOMS
  // ---------------------------------------
  [types.ROOMS_UPDATE] (state, room) {
    state.currentRoom = room
    if (state.rooms.length === 0) {
      state.rooms.push(room)
    } else {
      const recordIndex = state.rooms.findIndex((element) => {
        return element.roomName === room.roomName
      })
      if (recordIndex === -1) {
        state.rooms[recordIndex] = room
      } else {
        state.rooms.push(room)
      }
    }
  },

  // ---------------------------------------
  // NOTES
  // ---------------------------------------
  [types.NOTES_FILL] (state, elements) {
    state.notes = elements
  },
  [types.NOTES_UPDATE] (state, element) {
    let notes = state.notes
    const index = notes.findIndex((ele) => {
      return ele.id === element.id
    })
    if (index > -1) {
      notes[index] = element
      state.notes = []
      state.notes = notes
    }
  },
  [types.NOTES_DELETE] (state, id) {
    const index = state.notes.findIndex((ele) => {
      return ele.id === id
    })
    state.notes.splice(index, 1)
  },
  [types.NOTES_CREATED] (state, element) {
    state.notes.push(element)
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
