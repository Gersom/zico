import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import ajax from './modules/ajax'
import companies from './modules/companies'
import countries from './modules/countries'
import items from './modules/items'
import languages from './modules/languages'
import news from './modules/news'
import oauth from './modules/oauth'
import regions from './modules/regions'
import sectors from './modules/sectors'
import tags from './modules/tags'
import user from './modules/user'
import zicochat from './modules/zicochat'

import {sessionStoragePlugin} from './plugins/sessionStorage'
import {notificationPlugin} from './plugins/notification'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    ajax,
    companies,
    countries,
    items,
    languages,
    news,
    oauth,
    regions,
    sectors,
    tags,
    user,
    zicochat
  },
  strict: debug,
  plugins: debug ? [createLogger(), sessionStoragePlugin, notificationPlugin] : [sessionStoragePlugin, notificationPlugin]
})
