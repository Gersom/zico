import Home from 'App/screens/Home'
import FourHundredFour from 'App/screens/404'
import Languages from 'App/screens/Languages'
import AllNotFound from 'App/screens/AllNotFound'

const routes = [
  Home,
  FourHundredFour,
  Languages,
  AllNotFound
]

export default routes
